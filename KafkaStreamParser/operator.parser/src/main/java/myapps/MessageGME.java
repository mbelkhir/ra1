package myapps;

public class MessageGME {

    //Variables used to be send in REQ & ACK for GME messages   
    private String target;
    private Integer SN;
    private String dest;
    private String src;
    private String type;

    public MessageGME() {
    }

    public MessageGME(String target, Integer SN, String dest, String src, String type) {
        this.target = target;
        this.SN = SN;
        this.dest = dest;
        this.src = src;
        this.type = type;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getSN() {
        return SN;
    }

    public void setSN(Integer SN) {
        this.SN = SN;
    }

    public String getDest() {
        return dest;
    }

    public void setDest(String dest) {
        this.dest = dest;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        MessageGME guest = (MessageGME) obj;
        return (SN == guest.SN || (SN != null && SN.equals(guest.getSN())))
                && (target == guest.target || (target != null && target.equals(guest.getTarget())))
                && (dest == guest.dest || (dest != null && dest.equals(guest.getDest())))
                && (src == guest.src || (src != null && src.equals(guest.getSrc())))
                && (type == guest.type || (type != null && type.equals(guest.getType())));
    }

    @Override
    public String toString() {
        return "MessageGME{" + "target=" + target + ", SN=" + SN + ", dest=" + dest + ", src=" + src + ", type=" + type + '}';
    }

}
