package myapps;

import java.util.Objects;

public class Priority {

    public Integer x;
    public Integer y;

    public Priority(Integer x, Integer y) {
        this.x = x;
        this.y = y;
    }

    public Boolean lessThan(Priority priority) {
        if (y < priority.y) {
            return true;
        } else if (y > priority.y) {
            return false;
        } else if (Objects.equals(y, priority.y)) {
            if (x < priority.x) {
                return true;
            } else if (x > priority.x) {
                return false;
            } else {
                System.err.println("Error & exit (two priority are equals");
                System.exit(0);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
