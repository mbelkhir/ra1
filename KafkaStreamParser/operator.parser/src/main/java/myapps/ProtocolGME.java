package myapps;

import static java.lang.Integer.max;
import java.util.ArrayList;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class ProtocolGME {

    public void onReceiptOf(MessageGME msg, Repertory repertory, KafkaParserSecondNode node, Producer producer, Object ob) {

        synchronized (ob) {
            try {
                switch (msg.getType()) {

                    case "REQ": {
//                        System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                + " id:" + node.id + " RCV:REQ FROM:" + msg.getSrc()
//                                + " SN_i'j' = " + msg.getSN() + " priority:" + node.priority.y + " end");
                        Priority msgPriority = new Priority(Integer.parseInt(msg.getSrc()), msg.getSN());
                        node.SN = max(node.SN, msg.getSN());
                        if ((msgPriority.lessThan(node.priority)) || (KafkaParserSecondNode.target.equals(msg.getTarget()))) {
                            System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                                    + " id:" + node.id + " SEND:ACK_TO:" + msg.getSrc()
                                    + " SN_i'j' = " + msg.getSN() + " priority:" + node.priority.y + " end");
                            ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(msg.getSrc(),
                                    new MessageGME(node.target, null, msg.getSrc(), node.id + "", "ACK"));
                            producer.send(rec);
                        } else {
                            if (!node.requestSet.contains(msg)) {
                                node.requestSet.add(msg);
                            }
                        }
                        break;
                    }
                    case "ACK": {
//                        System.out.println("#time:" + (new java.util.Date().getTime() - node.startTime)
//                                + " id:" + node.id + " RCV:ACK FROM:" + msg.getSrc() + " end");
                        if (!node.acksSet.contains(msg)) {
                            node.acksSet.add(msg);
                        }
                        if (node.acksSet.size() == repertory.getNeighbors().size() - 1) {
                            System.out.println("time:" + (new java.util.Date().getTime() - node.startTime) + " id:"
                                    + node.id + " state:ENTER_CS " + "Group:" + node.target + " end");
                            node.state = "talking";
                            node.acksSet = new ArrayList<>();
                            ob.notify();
                        }
                    }
                    break;

                    default:
                        //System.err.println("message not recognized");
                        break;
                }
            } catch (Exception e) {
                Priority msgPriority = new Priority(Integer.parseInt(msg.getSrc()), msg.getSN());
                System.err.println(msgPriority.lessThan(node.priority));
                System.err.println("#time:" + (new java.util.Date().getTime() - node.startTime)
                        + " id:" + node.id + " RCV:REQ FROM:" + msg.getSrc()
                        + " SN_i'j' = " + msg.getSN() + " priority:" + node.priority.y + " end");
            }
        }
    }
}
