package myapps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author mbelkhir
 */
public class Repertory {

    private List<String> succsY = new ArrayList<>(Arrays.asList("12", "13", "14", "15", "16", "17"));
    private List<String> predsY = new ArrayList<>(Arrays.asList("0", "1", "2", "3", "4", "5"));
    private List<String> nodeY = new ArrayList<>(Arrays.asList("6", "7", "8", "9", "10", "11"));

    private List<String> succsX = new ArrayList<>(Arrays.asList("6", "7", "8", "9", "10", "11"));
    private List<String> predsX = new ArrayList<>(Arrays.asList());
    private List<String> nodeX = new ArrayList<>(Arrays.asList("0", "1", "2", "3", "4", "5"));

    private List<String> succsZ = new ArrayList<>(Arrays.asList());
    private List<String> predsZ = new ArrayList<>(Arrays.asList("6", "7", "8", "9", "10", "11"));
    private List<String> nodeZ = new ArrayList<>(Arrays.asList("12", "13", "14", "15", "16", "17"));

    private List<String> succs = new ArrayList<>(Arrays.asList());
    private List<String> preds = new ArrayList<>(Arrays.asList());

    private List<String> neighbors = new ArrayList<>(Arrays.asList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"));

    public List<String> getSuccs() {
        return succs;
    }

    public void setSuccs(List<String> succs) {
        this.succs = new ArrayList<>(succs);
    }

    public List<String> getPreds() {
        return preds;
    }

    public void setPreds(List<String> preds) {
        this.preds = new ArrayList<>(preds);
    }

    public List<String> getNeighbors() {
        return neighbors;
    }

    public List<String> getSuccsY() {
        return succsY;
    }

    public List<String> getPredsY() {
        return predsY;
    }

    public List<String> getSuccsX() {
        return succsX;
    }

    public List<String> getPredsX() {
        return predsX;
    }

}
