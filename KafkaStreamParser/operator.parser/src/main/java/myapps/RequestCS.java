package myapps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

/**
 *
 * @author mbelkhir
 */
public class RequestCS extends Thread {

    Repertory repertory;
    KafkaParserSecondNode node;
    Producer producer;
    Object o;
    Integer temp = 0;

    public RequestCS(Repertory repertory, KafkaParserSecondNode node, Producer producer, Object ob) {
        this.repertory = repertory;
        this.node = node;
        this.producer = producer;
        o = ob;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
            //Converger la vitesse de kafka consumer
            repertory.getNeighbors().stream().map((succ) -> {
                ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(succ,
                        new MessageGME("Hi", null, succ, node.id + "", "Hi"));
                return rec;
            }).forEachOrdered((rec) -> {
                producer.send(rec);
            });
            Thread.sleep(2000);
//            if (node.target.equals("X")) {
//                Thread.sleep(8000);
//            }
            while (true || node.target.equals("X")) {
                //int frequency = (int) (3000 + Math.random() * (30000 + 1));

                Thread.sleep(node.freqToRequestCS);

                if (temp >= node.tempOfSimulation) {
                    //send msg end
                    System.out.println("#finished");
                    sleep(20000);
                    System.exit(0);
                }
                synchronized (o) {                 
                    temp++;
                    node.state = "waiting";
                    node.SN++;
                    node.priority = new Priority(node.id, node.SN);
                    System.out.println("time:" + (new java.util.Date().getTime() - node.startTime)
                            + " id:" + node.id + " state:REQ " + "Group:" + node.target + " SN:" + node.SN + " end");
                    for (String nd : repertory.getNeighbors()) {
                        if (!nd.equals(node.id + "")) {
                            ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(nd,
                                    new MessageGME(node.target, node.priority.y, nd, node.id + "", "REQ"));
                            producer.send(rec);
                        }
                    }
                    // }
                    o.wait();
                }

                Thread.sleep(node.inCS);
                synchronized (o) {
                    System.out.println("time:" + (new java.util.Date().getTime() - node.startTime) + " id:"
                            + node.id + " state:LEAVE_CS " + "Group:" + node.target + " end");
                    Iterator<MessageGME> itr = node.requestSet.iterator();
                    while (itr.hasNext()) {
                        System.out.println("#SEND end");
                        MessageGME reqSet = itr.next();
                        ProducerRecord<String, MessageGME> rec = new ProducerRecord<>(reqSet.getSrc(),
                                new MessageGME(node.target, null, reqSet.getSrc(), node.id + "", "ACK"));
                        producer.send(rec);
                    }
                    node.requestSet = new ArrayList<>();
                    node.priority = new Priority(node.id, Integer.MAX_VALUE - 10000);
                    node.state = "thinking";

                }
            }
        } catch (InterruptedException ex) {
        }
    }
}
